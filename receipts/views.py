from django.shortcuts import render, redirect
from receipts.models import Receipt
from django.contrib.auth.decorators import login_required
from receipts.forms import ReceiptForm
# Create your views here.
# def ExpenseCategory(request, id):
#     expense = get_object_or_404(expense, id=id)
#     context = {
#         "expense_object": expense,
#     }
#     return render(request, "receipts/list.html", context)

# def Account(request, id):
#     account = get_object_or_404(account, id=id)
#     context = {
#         "account_object": account,
#     }
#     return render (request, "receipts/detail.html", context)
@login_required
def receipt_list(request):
    receipts = Receipt.objects.filter(purchaser=request.user)
    context = {
        "receipt_list": receipts,
    }
    return render(request, "receipts/list.html", context)

@login_required
def create_receipt(request):
    if request.method == "POST":
        form = ReceiptForm(request.POST)
        if form.is_valid():
            receipt = form.save(False)
            receipt.purchaser = request.user
            receipt.save()
            return redirect("home")
    else:
        form = ReceiptForm()
    context = {
        "form": form,
    }
    return render(request, "receipts/create.html", context)